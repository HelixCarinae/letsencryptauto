import os
import subprocess
import time

#Check if a LE folder exists and grabs the user's server name if it doesn't it quits
#NOTE MAKE SUITABLE FOR MORE THAN ONE SERVER PROBABLY SOME SORT OF FOR STATEMENT BASED OFF OF FOLDER CONTENTS
if os.path.exists("/etc/letsencrypt") == True:
    certPath = subprocess.check_output("ls /etc/letsencrypt/live/", shell=True)
else:
    print "You either don't have Let's Encrypt installed or have a different default folder"
    quit()

#Cleaning up the variable I think
certPath = str(certPath)
certPath = certPath.strip('0')
certPath = certPath.replace("\n", "")
#This is the full server's cert path.
serverPath = '/etc/letsencrypt/live/%s' % certPath
certLocation = "%s/cert.pem" % serverPath

#The above will let us check the last modified date for the file.
#The following grabs the stat command output of the file from bash and strips it down just a date that can be compared.
#lmd = last modified date
#calls stat info of cert
lmd = os.stat(certLocation).st_mtime
#Time passed between now and last modified date
timePass = int(time.time()) - lmd
#7,340,000 = 85 days in seconds
#silent mode
if timePass < int(7340000):
    quit()
if timePass >= int(7340000):
    print "You need to renew your cert. Starting Let's Encrypt."
    if os.path.exists("/usr/bin/letsencrypt") == True:
        if os.path.exists("/usr/share/doc/python-letsencrypt-apache") == True:
            os.system('sudo letsencrypt --apache')
            print "Done!"
            quit()
        elif os.path.exists("/usr/share/doc/python-letsencrypt-nginx") == True:
            os.system('sudo letsencrypt --nginx')
            print "Done!"
            quit()
        else:
            os.system('sudo letsencrypt')
            print "Done!"
            quit()
    else:
        print "You must not have Let's Encrypt"
        quit()
else:
    print "Your cert is up to date. Quitting"
    quit()